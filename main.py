import discord
import sqlite3
import sys
import datetime
import asyncio
import atexit
from discord.ext import commands
from typing import List, Dict, Tuple, Optional, Union, cast, Callable, Any # noqa: F401
from dataclasses import dataclass
from json import dumps, loads
import time


def handler():
    print("saving db")
    db.commit()
    db.close()


atexit.register(handler)

intent = discord.Intents.default()
intent.message_content = True
intent.reactions = True

NUKE_STYLE_REPLACE = "true_democracy"
NUKE_STYLE_EMPTY = "cia"
NUKE_STYLES = [NUKE_STYLE_REPLACE, NUKE_STYLE_EMPTY]

db = sqlite3.connect("database.db")
bot = commands.Bot("." if len(sys.argv) == 1 else ";", intents=intent)

ADMINS = "admins"
NUKE_STATES = "nkd_states"
PERRY_TICKETS = "prry_tckts"


@dataclass
class NukeState:
    chnl_id: int = 0
    last_tried: int = 0
    nuked: bool = False
    c_msg_id: int = 0
    style: int = 0

    @staticmethod
    def reset(dict):
        dict.nuked = False
        dict.last_tried = 0
        dict.c_msg_id = 0

    @staticmethod
    def nuke(dict):
        dict.nuked = True
        dict.last_tried = 0
        dict.c_msg_id = 0

    @staticmethod
    def validate(dict):
        pass
        # if 'style' not in dict:
        #     dict.style = 0

    def to_dict(self) -> dict:
        return self.__dict__


defaults = [
    (ADMINS, ""),  # comma seperated id
    (NUKE_STATES, '[]'),
    (PERRY_TICKETS, '[]'),
]


def setup() -> None:
    id = 1 if len(sys.argv) == 1 else 2
    print(id)

    cur = db.cursor()
    cur.execute("CREATE TABLE IF NOT EXISTS bot_tokens (id INTEGER PRIMARY KEY,token TEXT NOT NULL);")
    cur.execute("CREATE TABLE IF NOT EXISTS servers (guild INTEGER);")
    res = cur.execute("SELECT token FROM bot_tokens WHERE id = ?;", (id,))
    token = res.fetchone()
    if not token:
        print("Missing bot token in bot_tokens")
        exit(1)
    pass
    cur.close()
    bot.run(token[0])


class GuildSettings:
    def __init__(self, guild_id: int):
        self.guild_id = guild_id
        self.cur = db.cursor()
        # self.admins = None
        # self.nuke_state = None

    def __enter__(self):
        return self

    def __exit__(self, exc_type, exc_value, traceback):
        self.close()

    def close(self):
        self.cur.close()

    def get_setting(self, key: str):
        query = f"SELECT value FROM guild{self.guild_id} WHERE key = ?;"
        res = self.cur.execute(query, (key,))
        val = res.fetchone()
        if val:
            return val[0] if val[0] is not None else ""
        else:
            return ""

    def set_setting(self, key: str, val: str):
        cursor = self.cur.execute(f"UPDATE guild{self.guild_id} SET value = ? WHERE key = ?;", (val, key,))
        if cursor.rowcount == 0:
            _ = self.cur.execute(f'INSERT INTO guild{self.guild_id} (key,value) VALUES (?,?);', (key, val,))
        db.commit()

    @property
    def perry_tickets(self) -> Optional[List[dict]]:
        tickets = self.get_setting(PERRY_TICKETS)
        if tickets:
            return loads(tickets)
        return None

    @perry_tickets.setter
    def perry_tickets(self, vals: List[dict]) -> None:
        self.set_setting(PERRY_TICKETS, dumps(vals))

    @property
    def nuke_state(self) -> List[NukeState]:
        return [NukeState(**d) for d in loads(self.get_setting(NUKE_STATES))]

    @nuke_state.setter
    def nuke_state(self, vals: List[NukeState]):
        self.set_setting(NUKE_STATES, dumps([d.to_dict() for d in vals]))

    @property
    def admins(self) -> str:
        return self.get_setting(ADMINS)

    @admins.setter
    def admins(self, val: str):
        self.set_setting(ADMINS, val)

    def add_admin(self, val: int):
        admins = self.admins
        admins += "," + str(val)
        self.admins = admins

    def init(self, ctx: commands.Context):
        assert ctx.guild
        # TODO Check if guild already in servers
        _ = self.cur.execute("INSERT INTO servers (guild) VALUES (?);", (self.guild_id,))
        _ = self.cur.execute(f"CREATE TABLE IF NOT EXISTS guild{ctx.guild.id} (key TEXT, value TEXT);")
        for setting in defaults:
            self.cur.execute(f"INSERT INTO guild{self.guild_id} (key,value) VALUES (?,?);", (setting[0], setting[1],))
        db.commit()

    def is_init(self):
        res = self.cur.execute(f"SELECT * FROM guild{self.guild_id} LIMIT 1;")
        val = res.fetchone()
        if val:
            return True
        return False


class NukeCommands(commands.Cog):
    def __init__(self, bot):
        self.bot = bot

    @commands.command(name="test_nuke")
    async def test_nuke_cmd(self, ctx: commands.Context):
        """Attmepted to Nuke all nuke enabled channels on this server."""
        assert ctx.guild
        if not is_admin(ctx.guild.id, ctx.author.id):
            return
        await try_nuke_purge_space(ctx.guild.id)

    @commands.command(name="enable_nuke")
    async def enable_nuke_cmd(self, ctx: commands.Context):
        """Registers this channel to be regulately nuked."""
        assert ctx.guild
        if not is_admin(ctx.guild.id, ctx.author.id):
            return
        with GuildSettings(ctx.guild.id) as settings:
            # Get already registered states
            states: List[NukeState] = settings.nuke_state
            for state in states:
                # Make sure it's not already here
                if state.chnl_id == ctx.channel.id:
                    await ctx.channel.send("This channel is already setup to be nuked")
                    return
            # setup to nuke
            assert ctx.channel
            states.append(NukeState(ctx.channel.id))
            settings.nuke_state = states
            await ctx.channel.send("This channel is now setup to be nuked regularly. Enjoy your democracy.")

    @commands.command(name="disable_nuke")
    async def disable_nuke_cmd(self, ctx: commands.Context):
        """Un-registers this channel to be regulately nuked."""
        assert ctx.guild
        if not is_admin(ctx.guild.id, ctx.author.id):
            return
        with GuildSettings(ctx.guild.id) as settings:
            if not settings.is_init():
                await ctx.channel.send("Server is not initialized.")
                return

            # Get already registered states
            states: List[NukeState] = settings.nuke_state
            to_remove = None
            for state in states:
                if state.chnl_id == ctx.channel.id:
                    await ctx.channel.send("This channel will no longer be regularly nuked")
                    to_remove = state
                    break
            if to_remove:
                states.remove(to_remove)
                settings.nuke_state = states
        pass

    @commands.command(name="set_nuke_style")
    async def nuke_style_cmd(self, ctx: commands.Context, args):
        """[true_democracy. Delete and recreate] [cia. Try to delete all messages] [else. undefined]"""
        assert ctx.guild
        if not is_admin(ctx.guild.id, ctx.author.id):
            return
        with GuildSettings(ctx.guild.id) as settings:
            if not settings.is_init():
                await ctx.channel.send("Server is not initialized.")
                return
            # Get already registered states
            states: List[NukeState] = settings.nuke_state
            for state in states:
                # Make sure it's not already here
                if state.chnl_id == ctx.channel.id:
                    NukeState.validate(state)
                    state.style = args
                    settings.nuke_state = states
                    await ctx.channel.send(f"This channel is now set to nuke style {args}")
                    if args not in NUKE_STYLES:
                        await ctx.channel.send("...Though I'm not sure what that means. .help set_nuke_style for more details.")
                    return
            # setup to nuke
            await ctx.channel.send("This channel is setup to be nuked, so I can't set the style.")

    @commands.command(name="dupenuke")
    async def nuke_cmd(self, ctx: commands.Context):
        """Duplicates this channel, then deletes the original."""
        assert ctx.guild
        if not is_admin(ctx.guild.id, ctx.author.id):
            return
        assert isinstance(ctx.channel, discord.TextChannel)
        await dupe_and_nuke(ctx.channel, True)


class PerryCommands(commands.Cog):
    def __init__(self, bot):
        self.bot = bot

    @commands.command(name='create_perry_ticket')
    async def create_perry_ticket(self, ctx: commands.Context):
        '''Usage .create_perry_ticket Search is borked'''
        content = ctx.message.content[len('create_perry_ticket ') + 1:]
        ticket = {
            'created_on': datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S'),
            'created_by': ctx.author.display_name,
            'issue': content,
            'resolution': '',
        }
        assert ctx.guild
        with GuildSettings(ctx.guild.id) as settings:
            tickets: Optional[List[dict]] = settings.perry_tickets
            if not tickets:
                tickets = []
            tickets.append(ticket)
            settings.perry_tickets = tickets
            await ctx.channel.send("Submitted!")

    @commands.command(name='close_perry_ticket')
    async def close_perry_ticket(self, ctx: commands.Context):
        '''Usage .close_perry_ticket 69 WONT FIX'''
        assert ctx.guild
        if ctx.author.id != perry and not is_admin(ctx.guild.id, ctx.author.id):
            await ctx.channel.send('Only perry can close tickets')
            return
        parts: List[str] = ctx.message.content.split(' ')
        if len(parts) < 3:
            await ctx.channel.send('Proper syntax is: .close_perry_ticket ticket_number WONT FIX')
            return
        ticketid = 0
        try:
            ticketid = int(parts[1])
        except:
            await ctx.channel.send(f'{parts[1]} isn\'t a number')
            await ctx.channel.send('Proper syntax is: .close_perry_ticket ticket_number WONT FIX')
            return
        with GuildSettings(ctx.guild.id) as settings:
            tickets: Optional[List[dict]] = settings.perry_tickets
            if not tickets or ticketid >= len(tickets):
                await ctx.channel.send(f'There are only {len(tickets) if tickets else "No"} tickets.')
                return
            tickets[ticketid]['resolution'] = ' '.join(parts[2:])
            settings.perry_tickets = tickets
            await ctx.channel.send('Ticket marked as resolved')

    @commands.command(name='lookup_perry_ticket')
    async def lookup_perry_ticket(self, ctx: commands.Context):
        '''Usage .lookup_perry_ticket 129'''
        ticketid_str = ctx.message.content[len("lookup_perry_ticket ") + 1:]
        ticketid = 0
        assert ctx.guild
        with GuildSettings(ctx.guild.id) as settings:
            tickets: Optional[List[dict]] = settings.perry_tickets
            try:
                ticketid = int(ticketid_str)
            except:
                if not ticketid_str:
                    if tickets:
                        await ctx.channel.send(f'There are {len(tickets)} tickets.')
                    else:
                        await ctx.channel.send('There are no tickets yet.')
                    return
                await ctx.channel.send(f"{ticketid} isn\'t a number.")
                return
            if not tickets or ticketid >= len(tickets):
                await ctx.channel.send(f"There are only {len(tickets) if tickets else 'No'} tickets.")
                return
            ticket = tickets[ticketid]
            is_resolved = ticket['resolution']
            desc = f'''created by: {ticket['created_by']}
created on: {ticket['created_on']}
issue: {ticket['issue']}
{f'Resolved: {is_resolved}' if is_resolved else ''}
'''
            embed = discord.Embed(color=0x3399DD if not is_resolved else 0x33DD99, title=f'Ticket #{ticketid}', description=desc)
            await ctx.channel.send(embed=embed)


class AdministrativeCommands(commands.Cog):
    def __init__(self, bot):
        self.bot = bot

    @commands.command(name='set_welcome_msg')
    async def set_welcome_msg(self, ctx: commands.Context):
        """Please read command name"""
        assert ctx.guild
        if not is_admin(ctx.guild.id, ctx.author.id):
            return
        with GuildSettings(ctx.guild.id) as settings:
            settings.set_setting('welcome_msg', ctx.message.content[17:])
            await ctx.channel.send('Saved.')

    @commands.command(name='welcome')
    async def send_welcome_msg(self, ctx: commands.Context):
        """Sends a welcome message to the mentioned user"""
        assert ctx.guild
        if not is_admin(ctx.guild.id, ctx.author.id):
            return
        with GuildSettings(ctx.guild.id) as settings:
            msg = settings.get_setting('welcome_msg')
            if not msg:
                await ctx.channel.send('No welcome message set.')
                return
            # await ctx.channel.send('Saved.')
            mentions = ctx.message.raw_mentions
            if len(mentions) == 0:
                await ctx.channel.send('Who? you didn\'t mention anyone to welcome.')
                return
            await ctx.channel.send(msg.replace(r"%user", f'<@{mentions[0]}>'))
            await ctx.message.delete()

    @commands.command(name="is_admin")
    async def is_admin_cmd(self, ctx: commands.Context):
        """Lets you know if you are permitted to execute priviliaged commands."""
        assert ctx.guild
        await ctx.channel.send(str(is_admin(ctx.guild.id, ctx.author.id)))

    @commands.command(name="init_server")
    async def init_server_cmd(self, ctx: commands.Context):
        assert ctx.guild
        if not is_admin(ctx.guild.id, ctx.author.id):
            return
        with GuildSettings(ctx.guild.id) as settings:
            settings.init(ctx)
            await ctx.channel.send("Server is ready! :happy:")

    @commands.command(name="make_admin")
    async def make_admin_cmd(self, ctx: commands.Context):
        assert ctx.guild
        if not is_admin(ctx.guild.id, ctx.author.id):
            return
        with GuildSettings(ctx.guild.id) as settings:
            if not settings.is_init():
                await ctx.channel.send("Server not initialized")
                return
            admins = settings.admins
            admins += "," + ",".join(list(map(lambda m: str(m.id), ctx.message.mentions)))
            settings.admins = admins
            await ctx.channel.send("Success!")
            return


@bot.command(name="hello")
async def hello_cmd(ctx: commands.Context):
    """Test to make sure the bot is alive. Will Respond with Pong"""
    await ctx.channel.send("Pong")


@bot.command("order_fries")
async def order_fries(ctx: commands.Context):
    """As requested in the requirements letter."""
    await ctx.channel.send("Sorry our Ice cream machine is broken.")


# @bot.command(name="quote")
# async def quote_cmd(ctx: commands.Context):
#     pass


async def nuke_action(state: NukeState, channel: discord.TextChannel):
    if state.style == NUKE_STYLE_REPLACE:
        NukeState.nuke(state)
        state.chnl_id = await dupe_and_nuke(channel, True)
    elif state.style == NUKE_STYLE_EMPTY:
        NukeState.nuke(state)
        for _ in range(50):
            messages = [msg async for msg in channel.history()]
            if len(messages) == 0:
                break
            await channel.delete_messages(messages)
            if len(messages) < 100:
                break
    else:
        await channel.send(f"Unknown nuke style {state.style}")


async def regular_nuke() -> None:
    await bot.wait_until_ready()
    cur = db.cursor()
    while True:
        # TODO
        res = cur.execute("SELECT guild FROM servers;")
        servers: List[Tuple] = res.fetchall()
        print("Checking regular nuke")
        print("servers: " + str(servers))
        print("weekday:" + str(datetime.datetime.now().isoweekday()))
        if bot.is_ready() and datetime.datetime.now().isoweekday() in [3, 7] and servers:
            print("It's nuking time!")
            for server in servers:
                print("nuking: " + str(server))
                await try_nuke_purge_space(server[0])
            await asyncio.sleep(60 * 60) # 1hour
        elif servers:
            print("offday")
            for guildid in servers:
                print("Checking " + str(guildid[0]))
                with GuildSettings(guildid[0]) as settings:
                    states: List[NukeState] = settings.nuke_state
                    for state in states:
                        NukeState.reset(state)
                        print("resetting state:")
                        print(state)
                    settings.nuke_state = states
        await asyncio.sleep(12 * 60 * 60) # 12hours

biom4st3r = 299255914916085761
perry = 266376928288571394


# channel, message
awaiting_approval: Dict[int, int] = {}


async def try_nuke_purge_space(guildid: int) -> None:
    global PURGE_EMOJI
    guild = bot.get_guild(guildid)
    assert guild
    with GuildSettings(guildid) as settings:
        states: List[NukeState] = settings.nuke_state
        for state in states:
            NukeState.validate(state)
            now = int(datetime.datetime.now().timestamp())
            if state.nuked:
                print(state)
                print(f"already nuked: {state.nuked}")
                continue
            if now - state.last_tried < 60 * 60 and state.last_tried != 0:
                print("already tried recently: ")
                continue
            channel = guild.get_channel(state.chnl_id)
            if not isinstance(channel, discord.TextChannel):
                print("Registered Nuke Channel is NOT a text Channel")
                return
            # msg: discord.Message = channel.last_message
            msgs = [m async for m in channel.history(limit=1)]
            if len(msgs) == 0:
                print("Channel appears empty. skipping nuke.")
                continue
            latest_msg = msgs[0]
            # print(msg.content)
            sent = int(latest_msg.created_at.timestamp())
            now = int(time.time())
            diff = now - sent
            minutes_since = diff / 60
            print("minutes since last: " + str(minutes_since))
            if minutes_since < 60:
                msg: discord.Message = await channel.send("I was going to reset this channel, but it looks like you're still chatting. React with :thumbsup: to purge anyways")
                await msg.add_reaction(PURGE_EMOJI)
                state.last_tried = int(time.time())
                state.c_msg_id = msg.id
            else:
                await nuke_action(state, channel)

        settings.nuke_state = states


def is_admin(guild_id: int, id: int) -> bool:
    if id == biom4st3r:
        return True
    cur = db.cursor()
    res = cur.execute(
        f"SELECT value FROM guild{guild_id} WHERE key = '{ADMINS}' AND (value LIKE ? OR value LIKE ? OR value LIKE ?);",
        (f'%,{str(id)},%', f'{str(id)},%', f'%,{str(id)}',)
    )
    val = res.fetchone()
    isa = False
    if not val:
        isa = False
    else:
        isa = True
    cur.close()
    return isa


@bot.event
async def on_ready():
    await bot.add_cog(AdministrativeCommands(bot))
    await bot.add_cog(NukeCommands(bot))
    await bot.add_cog(PerryCommands(bot))
    bot.loop.create_task(regular_nuke())
    print(f"Connected {bot.user}!")


async def dupe_and_nuke(channel: discord.TextChannel, nuke: bool = False) -> int:
    assert channel.topic is not None
    # assert channel.overwrites is not None
    new_channel: discord.TextChannel = await channel.guild.create_text_channel(
        channel.name, reason=None, category=channel.category,
        news=channel.is_news(), position=channel.position, topic=channel.topic,
        slowmode_delay=channel.slowmode_delay, nsfw=channel.is_nsfw(), overwrites=channel.overwrites, # type: ignore[arg-type]
        default_auto_archive_duration=channel.default_auto_archive_duration, default_thread_slowmode_delay=channel.default_thread_slowmode_delay,
    )
    if nuke:
        await channel.delete()
    return new_channel.id


@bot.event
async def on_message(msg: discord.Message):
    if msg.author == bot.user:
        print("Ignore self")
        return
    await bot.process_commands(msg)
    assert bot.user
    if bot.user.mentioned_in(msg):
        print(f"@bot -> {msg.content}")


PURGE_EMOJI = '👍'


@bot.event
async def on_reaction_add(reaction: discord.Reaction, user: discord.User):
    global PURGE_EMOJI
    # If the message the reaction is on is from me
    # Ignore it
    if reaction.message.author != bot.user:
        print('Ignoring Self React')
        return
    if reaction.emoji == PURGE_EMOJI:
        users: List[Union[discord.User, discord.Member]] = [user async for user in reaction.users(limit=2)]
        if len(users) != 2:
            return
        cid = reaction.message.channel.id
        assert reaction.message.guild

        settings = GuildSettings(reaction.message.guild.id)
        if not settings.is_init():
            return
        states: List[NukeState] = settings.nuke_state
        for state in states:
            if cid != state.chnl_id:
                continue
            if state.nuked:
                break
            NukeState.nuke(state)
            assert isinstance(reaction.message.channel, discord.TextChannel)
            await nuke_action(state, reaction.message.channel)
            # state['chnl_id'] = await dupe_and_nuke(reaction.message.channel, True)
            # settings.nuke_state = states
            break
        settings.nuke_state = states

setup()
